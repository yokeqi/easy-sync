﻿namespace EasyFileSync.Win
{
    partial class FrmLocal2Local
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbSrc = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSelectTar = new System.Windows.Forms.Button();
            this.btnSelectSrc = new System.Windows.Forms.Button();
            this.tbTar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rb_mode_append = new System.Windows.Forms.RadioButton();
            this.rb_mode_mirror = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rb_strategy_hash = new System.Windows.Forms.RadioButton();
            this.rb_strategy_size = new System.Windows.Forms.RadioButton();
            this.rb_strategy_date = new System.Windows.Forms.RadioButton();
            this.btnStart = new System.Windows.Forms.Button();
            this.fbdSelect = new System.Windows.Forms.FolderBrowserDialog();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbSrc
            // 
            this.tbSrc.BackColor = System.Drawing.Color.White;
            this.tbSrc.Location = new System.Drawing.Point(59, 20);
            this.tbSrc.Name = "tbSrc";
            this.tbSrc.ReadOnly = true;
            this.tbSrc.Size = new System.Drawing.Size(400, 21);
            this.tbSrc.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSelectTar);
            this.groupBox1.Controls.Add(this.btnSelectSrc);
            this.groupBox1.Controls.Add(this.tbTar);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbSrc);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(534, 80);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "路径";
            // 
            // btnSelectTar
            // 
            this.btnSelectTar.Location = new System.Drawing.Point(465, 47);
            this.btnSelectTar.Name = "btnSelectTar";
            this.btnSelectTar.Size = new System.Drawing.Size(43, 23);
            this.btnSelectTar.TabIndex = 5;
            this.btnSelectTar.Text = "选择";
            this.btnSelectTar.UseVisualStyleBackColor = true;
            this.btnSelectTar.Click += new System.EventHandler(this.btnSelectTar_Click);
            // 
            // btnSelectSrc
            // 
            this.btnSelectSrc.Location = new System.Drawing.Point(465, 18);
            this.btnSelectSrc.Name = "btnSelectSrc";
            this.btnSelectSrc.Size = new System.Drawing.Size(43, 23);
            this.btnSelectSrc.TabIndex = 4;
            this.btnSelectSrc.Text = "选择";
            this.btnSelectSrc.UseVisualStyleBackColor = true;
            this.btnSelectSrc.Click += new System.EventHandler(this.btnSelectSrc_Click);
            // 
            // tbTar
            // 
            this.tbTar.BackColor = System.Drawing.Color.White;
            this.tbTar.Location = new System.Drawing.Point(59, 47);
            this.tbTar.Name = "tbTar";
            this.tbTar.ReadOnly = true;
            this.tbTar.Size = new System.Drawing.Size(400, 21);
            this.tbTar.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "目标：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "来源：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rb_mode_append);
            this.groupBox2.Controls.Add(this.rb_mode_mirror);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 80);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(534, 50);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "模式";
            // 
            // rb_mode_append
            // 
            this.rb_mode_append.AutoSize = true;
            this.rb_mode_append.Location = new System.Drawing.Point(67, 20);
            this.rb_mode_append.Name = "rb_mode_append";
            this.rb_mode_append.Size = new System.Drawing.Size(47, 16);
            this.rb_mode_append.TabIndex = 0;
            this.rb_mode_append.Text = "增量";
            this.rb_mode_append.UseVisualStyleBackColor = true;
            // 
            // rb_mode_mirror
            // 
            this.rb_mode_mirror.AutoSize = true;
            this.rb_mode_mirror.Checked = true;
            this.rb_mode_mirror.Location = new System.Drawing.Point(14, 20);
            this.rb_mode_mirror.Name = "rb_mode_mirror";
            this.rb_mode_mirror.Size = new System.Drawing.Size(47, 16);
            this.rb_mode_mirror.TabIndex = 0;
            this.rb_mode_mirror.TabStop = true;
            this.rb_mode_mirror.Text = "镜像";
            this.rb_mode_mirror.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rb_strategy_hash);
            this.groupBox3.Controls.Add(this.rb_strategy_size);
            this.groupBox3.Controls.Add(this.rb_strategy_date);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 130);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(534, 50);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "策略";
            // 
            // rb_strategy_hash
            // 
            this.rb_strategy_hash.AutoSize = true;
            this.rb_strategy_hash.Checked = true;
            this.rb_strategy_hash.Location = new System.Drawing.Point(168, 20);
            this.rb_strategy_hash.Name = "rb_strategy_hash";
            this.rb_strategy_hash.Size = new System.Drawing.Size(83, 16);
            this.rb_strategy_hash.TabIndex = 1;
            this.rb_strategy_hash.TabStop = true;
            this.rb_strategy_hash.Text = "文件哈希值";
            this.rb_strategy_hash.UseVisualStyleBackColor = true;
            // 
            // rb_strategy_size
            // 
            this.rb_strategy_size.AutoSize = true;
            this.rb_strategy_size.Location = new System.Drawing.Point(91, 20);
            this.rb_strategy_size.Name = "rb_strategy_size";
            this.rb_strategy_size.Size = new System.Drawing.Size(71, 16);
            this.rb_strategy_size.TabIndex = 0;
            this.rb_strategy_size.Text = "文件大小";
            this.rb_strategy_size.UseVisualStyleBackColor = true;
            // 
            // rb_strategy_date
            // 
            this.rb_strategy_date.AutoSize = true;
            this.rb_strategy_date.Location = new System.Drawing.Point(14, 20);
            this.rb_strategy_date.Name = "rb_strategy_date";
            this.rb_strategy_date.Size = new System.Drawing.Size(71, 16);
            this.rb_strategy_date.TabIndex = 0;
            this.rb_strategy_date.Text = "修改日期";
            this.rb_strategy_date.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(230, 186);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 40);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "开始";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // rtbLog
            // 
            this.rtbLog.BackColor = System.Drawing.Color.White;
            this.rtbLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLog.Location = new System.Drawing.Point(0, 0);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.ReadOnly = true;
            this.rtbLog.Size = new System.Drawing.Size(534, 322);
            this.rtbLog.TabIndex = 5;
            this.rtbLog.Text = "";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.btnStart);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rtbLog);
            this.splitContainer1.Size = new System.Drawing.Size(534, 561);
            this.splitContainer1.SplitterDistance = 235;
            this.splitContainer1.TabIndex = 6;
            // 
            // FrmLocal2Local
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 561);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLocal2Local";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "本地文件拷贝";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbSrc;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbTar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectSrc;
        private System.Windows.Forms.Button btnSelectTar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rb_mode_mirror;
        private System.Windows.Forms.RadioButton rb_mode_append;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rb_strategy_hash;
        private System.Windows.Forms.RadioButton rb_strategy_size;
        private System.Windows.Forms.RadioButton rb_strategy_date;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.FolderBrowserDialog fbdSelect;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}