﻿using EasyFileSync.Core;
using EasyFileSync.Core.Config;
using EasyFileSync.Core.Entity;
using EasyFileSync.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyFileSync.Win
{
    public partial class FrmLocal2Local : Form
    {
        public FrmLocal2Local()
        {
            InitializeComponent();
        }

        private void btnSelectSrc_Click(object sender, EventArgs e)
        {
            if (fbdSelect.ShowDialog() == DialogResult.OK)
            {
                tbSrc.Text = fbdSelect.SelectedPath;
            }
        }

        private void btnSelectTar_Click(object sender, EventArgs e)
        {
            if (fbdSelect.ShowDialog() == DialogResult.OK)
            {
                tbTar.Text = fbdSelect.SelectedPath;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            ThreadPool.GetMaxThreads(out int wt, out int cpt);
            ThreadPool.SetMaxThreads(1000, 200);
            try
            {
                btnStart.Enabled = false;
                rtbLog.Clear();
                var path_src = tbSrc.Text;
                if (string.IsNullOrWhiteSpace(path_src) || !Directory.Exists(path_src))
                {
                    MessageBox.Show("请先选择有效的来源文件夹。");
                    return;
                }
                var path_tar = tbTar.Text;
                if (string.IsNullOrWhiteSpace(path_tar) || !Directory.Exists(path_tar))
                {
                    MessageBox.Show("请先选择有效的目标文件夹。");
                    return;
                }

                var config = new JobConfig()
                {
                    Name = "temp.",
                    From = new NTFSDir(path_src),
                    To = new NTFSDir(path_tar),
                    Mode = rb_mode_mirror.Checked ? SyncMode.Mirror : SyncMode.Append,
                    Strategy = rb_strategy_date.Checked ? SyncStrategy.Date : (rb_strategy_size.Checked ? SyncStrategy.Size : SyncStrategy.HashCode),
                    Type = SyncType.LocalToLocal
                };

                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var job = new SyncJob(config);
                        job.OutStream += Job_OutStream;
                        job.Start();
                    }
                    finally
                    {
                        this.BeginInvoke(new Action(() =>
                        {
                            btnStart.Enabled = true;
                            ThreadPool.SetMaxThreads(wt, cpt);
                        }));
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                btnStart.Enabled = true;
                ThreadPool.SetMaxThreads(wt, cpt);
            }
        }

        private void Job_OutStream(string text)
        {
            this.BeginInvoke(new Action<string>((log) =>
            {
                rtbLog.AppendText(log);
                rtbLog.AppendText(Environment.NewLine);
            }), text);
        }
    }
}
