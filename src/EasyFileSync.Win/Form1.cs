﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyFileSync.Win
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnlocal2local_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new FrmLocal2Local())
            {
                form.ShowDialog();
            }
            this.Show();
        }
    }
}
